//
//  FeedInteractorTests.swift
//  SampleProjectTests
//
//  Created by Emre Cakirlar on 6/16/20.
//  Copyright © 2020 emrewtc. All rights reserved.
//

import XCTest
@testable import SampleProject

class FeedInteractorTests: XCTestCase {
	private var feedInteractor: FeedInteracting!

    override func setUpWithError() throws {
		self.feedInteractor = FeedInteractor(with: FeedApiClientMock())
    }

    func testFetchFeed() throws {
		let expectation = XCTestExpectation(description: "testFetchFeed")
		self.feedInteractor.didFetchFeed = { _, _ in
			expectation.fulfill()
		}
		self.feedInteractor.fetchFeed(for: .public)
		wait(for: [expectation], timeout: 5)
    }
}

// MARK: - Mocks

private struct FeedApiClientMock: FeedApiClientProtocol {
	var session: NetworkSession = MockNetworkSession()
	func getFeed(for tag: FeedType, completion: @escaping (Response<Feed>) -> Void) -> TaskCancelable? {
		let feed = Feed(items: [], modifiedDate: Date())
		completion(.success(feed))
		return nil
	}
}
