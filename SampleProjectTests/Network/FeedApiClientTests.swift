//
//  FeedApiClientTests.swift
//  SampleProjectTests
//
//  Created by Emre Cakirlar on 5/2/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import XCTest
@testable import SampleProject

class FeedApiClientTests: XCTestCase {
	let mockSession = MockNetworkSession()
	var apiClient: FeedApiClientProtocol!
	
	
	override func setUp() {
		self.apiClient = FeedApiClient(session: self.mockSession)
	}
	
	override func tearDown() {
		self.mockSession.removeStubs()
	}
	
	func testGetPublicFeed() {
		NetworkStubController.shared.injectStub(.publicFeed, for: self.mockSession)
		let apiExpectation = XCTestExpectation(description: "testGetPublicFeed")

		self.apiClient.getFeed(for: .public) { response in
			switch response {
			case .success(let feed):
				XCTAssertNotNil(feed.items[0].media.m)
			case .error(let error):
				XCTFail(error.localizedDescription)
			}
			apiExpectation.fulfill()
		}
		wait(for: [apiExpectation], timeout: 5)
	}
	
	func testGetFeedForTag() {
		NetworkStubController.shared.injectStub(.tag, for: self.mockSession)
		let apiExpectation = XCTestExpectation(description: "testGetFeedForTag")
		
		self.apiClient.getFeed(for: .cats) { response in
			switch response {
			case .success(let feed):
				XCTAssertNotNil(feed.items[0].media.m)
			case .error(let error):
				XCTFail(error.localizedDescription)
			}
			apiExpectation.fulfill()
		}
		wait(for: [apiExpectation], timeout: 5)
	}
	
}
