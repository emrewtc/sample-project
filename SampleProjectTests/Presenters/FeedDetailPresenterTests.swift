//
//  FeedDetailPresenterTests.swift
//  SampleProjectTests
//
//  Created by Emre Cakirlar on 6/16/20.
//  Copyright © 2020 emrewtc. All rights reserved.
//

import XCTest
@testable import SampleProject

class FeedDetailPresenterTests: XCTestCase {
	private let photoStub = Photo(title: "Photo1", link: URL(string: "photo1_url")!, media: .init(m: URL(string: "media1_url")!), takenDate: Date())
	private var feedRouter: FeedRouterMock!
	private var feedDetailPresenter: FeedDetailPresenting!

    override func setUpWithError() throws {
		self.feedRouter = FeedRouterMock()
		self.feedDetailPresenter = FeedDetailPresenter(with: self.feedRouter, viewModel: self.photoStub)
    }

    func testGetTitle() throws {
		XCTAssert(self.feedDetailPresenter.getTitle() == self.photoStub.title)
    }

	func testGetImageURL() throws {
		XCTAssert(self.feedDetailPresenter.getImageURL() == self.photoStub.media.m)
	}

	func testDidClickViewOnFlickrButton() throws {
		self.feedDetailPresenter.didClickViewOnFlickrButton()
		XCTAssert(self.feedRouter.isDidClickViewOnFlickrButtonCalled, "didClickViewOnFlickrButton is expected to call!")
	}

}

// MARK: - Mocks

private final class FeedRouterMock: FeedRouting {
	var isDidClickViewOnFlickrButtonCalled = false

	func start() {}
	func didClickFeedItem(_ item: Photo) {}
	func didClickViewOnFlickrButton(_ url: URL) {
		isDidClickViewOnFlickrButtonCalled = true
	}
}


