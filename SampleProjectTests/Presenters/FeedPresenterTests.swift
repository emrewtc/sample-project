//
//  FeedPresenterTests.swift
//  SampleProjectTests
//
//  Created by Emre Cakirlar on 6/16/20.
//  Copyright © 2020 emrewtc. All rights reserved.
//

import XCTest
@testable import SampleProject

class FeedPresenterTests: XCTestCase {
	private let now = Date()
	private var feedRouter: FeedRouterMock!
	private var feedInteractorOutput: FeedInteractorOutputMock!
	private var feedPresenter: FeedPresenting!


    override func setUpWithError() throws {
		self.feedRouter = FeedRouterMock()
		self.feedInteractorOutput = FeedInteractorOutputMock()
		self.feedPresenter = FeedPresenter(with: self.feedRouter, interactor: self.feedInteractorOutput)
		self.feedInteractorOutput.didFetchFeed(.success(self.makeFakeFeed()), .public)
    }

    func testDidUpdateFeed() throws {
		let expectation = XCTestExpectation(description: "testDidUpdateFeed")
		self.feedPresenter.didUpdateFeed = { _ in
			expectation.fulfill()
		}
		self.feedInteractorOutput.didFetchFeed(.success(self.makeFakeFeed()), .public)
		wait(for: [expectation], timeout: 5)
    }

    func testDidFailUpdatingFeed() throws {
		let expectation = XCTestExpectation(description: "testDidFailUpdatingFeed")
		self.feedPresenter.didFailUpdatingFeed = { _, _ in
			expectation.fulfill()
		}
		self.feedInteractorOutput.didFetchFeed(.failure(NSError()), .public)
		wait(for: [expectation], timeout: 5)
    }

	func testDidClickFeedItem() throws {
		self.feedPresenter.didClickFeedItem(self.makePhotos().first!)
		XCTAssert(self.feedRouter.isDidClickFeedItemCalled, "didClickFeedItem is expected to call!")
	}

	func testGetPhotoCountForType() throws {
		XCTAssert(self.feedPresenter.getPhotoCount(for: .public) == self.makePhotos().count)
	}

	func testGetPhotosForType() throws {

		let photos = self.feedPresenter.getPhotos(for: .public)
		let stubPhotos = self.makeFakeFeed().items
		XCTAssert(photos.count == stubPhotos.count)
		XCTAssert(photos.first?.title == stubPhotos.first?.title)
		XCTAssert(photos.first?.link == stubPhotos.first?.link)
		XCTAssert(photos.first?.media.m == stubPhotos.first?.media.m)
		XCTAssert(photos.first?.takenDate == stubPhotos.first?.takenDate)
	}

	func testGetLastUpdateAt() throws {
		let formatter = DateFormatter()
		formatter.dateFormat = "MMM d, h:mm a"
		XCTAssert(self.feedPresenter.getLastUpdateAt() == formatter.string(from: self.makeFakeFeed().modifiedDate))
	}

	// MARK: - Stubs

	private func makeFakeFeed() -> Feed {
		Feed(items: self.makePhotos(), modifiedDate: self.now)
	}

	private func makePhotos() -> [Photo] {
		[
			Photo(title: "Photo1", link: URL(string: "photo1_url")!, media: .init(m: URL(string: "media1_url")!), takenDate: self.now),
			Photo(title: "Photo2", link: URL(string: "photo2_url")!, media: .init(m: URL(string: "media2_url")!), takenDate: self.now),
			Photo(title: "Photo3", link: URL(string: "photo3_url")!, media: .init(m: URL(string: "media3_url")!), takenDate: self.now)
		]
	}
}


// MARK: - Mocks

private final class FeedInteractorOutputMock: FeedInteractingOutput {
	var didFetchFeed: (Result<Feed, Error>, FeedType) -> Void = { _, _ in }
}

private final class FeedRouterMock: FeedRouting {
	var isDidClickFeedItemCalled = false

	func start() {}
	func didClickViewOnFlickrButton(_ url: URL) {}

	func didClickFeedItem(_ item: Photo) {
		self.isDidClickFeedItemCalled = true
	}
}
