//
//  NetworkStubController.swift
//  SampleProjectTests
//
//  Created by Emre Cakirlar on 5/2/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation
@testable import SampleProject

enum Stub: String {
	case publicFeed = "publicFeed.json"
	case tag = "cats.json"
}

struct NetworkStubController {
	static let shared: NetworkStubController = NetworkStubController()
	private init() { }
	
	func injectStub(_ stub: Stub, for session: MockNetworkSession) {
		let fileName = stub.rawValue as NSString
		guard let path = Bundle.main.path(forResource: fileName.deletingPathExtension, ofType: fileName.pathExtension),
			let text = try? String(contentsOfFile: path),
			let data = text.data(using: .utf8) else {
				print("NO FILE IS FOUND")
				return
		}
		session.setStubData(data)
	}
	
}
