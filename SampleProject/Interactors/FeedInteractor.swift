//
//  FeedInteractor.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol FeedInteracingInput {
	func fetchFeed(for type: FeedType)
}

protocol FeedInteractingOutput {
	var didFetchFeed: (Result<Feed, Error>, FeedType) -> Void { get set }
}

typealias FeedInteracting = FeedInteracingInput & FeedInteractingOutput

final class FeedInteractor: FeedInteracting {
	var didFetchFeed: (Result<Feed, Error>, FeedType) -> Void = { _, _  in }
	private let apiClient: FeedApiClientProtocol
	
	init(with apiClient: FeedApiClientProtocol) {
		self.apiClient = apiClient
	}
	
	func fetchFeed(for type: FeedType = .public) {
		self.getFeedFor(type)
	}

	private func getFeedFor(_ type: FeedType) {
		self.apiClient.getFeed(for: type) { [unowned self] response in
			switch response {
			case .success(let feed):
				self.didFetchFeed(.success(feed), type)
			case .error(let error):
				self.didFetchFeed(.failure(error), type)
			}
		}
	}
	
}
