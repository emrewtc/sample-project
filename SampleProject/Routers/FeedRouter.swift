//
//  FeedRouter.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit
import SafariServices

protocol FeedRouterDelegate: class {
	func didClickFeedItem(_ item: Photo)
	func didClickViewOnFlickrButton(_ url: URL)
}

final class FeedRouter: AppRouting {
	let window: UIWindow
	var launchOptions: LaunchOptions?
	private var root: UINavigationController?
	
	init(with window: UIWindow) {
		self.window = window
	}
	
	func start(with launchOptions: LaunchOptions?) {
		let feedApiClient = FeedApiClient()
		let feedInteractor = FeedInteractor(with: feedApiClient)
		let feedPresenter = FeedPresenter(with: self, interactor: feedInteractor)
		let feedHub = FeedHub(with: feedInteractor, presenter: feedPresenter)
		let feedViewController = FeedViewController(with: feedHub)
		let navigationController = UINavigationController(rootViewController: feedViewController)
		self.window.rootViewController = navigationController
		self.window.makeKeyAndVisible()
		self.root = navigationController
		self.launchOptions = launchOptions
	}
}

extension FeedRouter: FeedRouterDelegate {
	func didClickViewOnFlickrButton(_ url: URL) {
		let inAppBrowser = SFSafariViewController(url: url)
		self.root?.present(inAppBrowser, animated: true, completion: nil)
	}
	
	func didClickFeedItem(_ item: Photo) {
		let feedDetailPresenter = FeedDetailPresenter(with: self, viewModel: item)
		let feedDetailHub = FeedDetailHub(with: feedDetailPresenter)
		let feedDetailViewController = FeedDetailViewController(with: feedDetailHub)
		self.root?.show(feedDetailViewController, sender: self.root)
	}
	
}
