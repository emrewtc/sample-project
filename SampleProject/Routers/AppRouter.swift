//
//  AppRouter.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class AppRouter: AppRouting {
	private enum AppRouterType {
		case feed
	}
	
	let window: UIWindow
	var launchOptions: LaunchOptions?
	private var childRouters: [AppRouterType: AppRouting] = [:]
	
	init(with window: UIWindow) {
		self.window = window
	}
	
	func start(with launchOptions: LaunchOptions?) {
		let feedRouter = FeedRouter(with: self.window)
		feedRouter.start(with: launchOptions)
		self.childRouters[.feed] = feedRouter
	}
}
