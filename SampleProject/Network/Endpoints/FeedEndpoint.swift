//
//  FeedEndpoint.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

enum FeedEndpoint {
	case tag(FeedType)
}

extension FeedEndpoint: EndpointType {
	var path: String {
		switch self {
		default:
			return "photos_public.gne"
		}
	}
	
	var queryParameters: [String: String?]? {
		switch self {
		case .tag(let type):
			return [
				"format": "json",
				"nojsoncallback": "1",
				"tags": type.tag
			]
		}
	}
}
