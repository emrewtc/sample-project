//
//  FeedApiClient.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol FeedApiClientProtocol: ApiClient {
	@discardableResult
	func getFeed(for tag: FeedType, completion: @escaping (Response<Feed>) -> Void) -> TaskCancelable?
}

struct FeedApiClient: FeedApiClientProtocol {
	let session: NetworkSession
	
	init(session: NetworkSession = URLSession.shared) {
		self.session = session
	}
	
	@discardableResult
	func getFeed(for tag: FeedType, completion: @escaping (Response<Feed>) -> Void) -> TaskCancelable? {
		let endpoint = FeedEndpoint.tag(tag)
		let dispatcher = Dispatcher<Feed>(with: self)
		return dispatcher.request(with: endpoint, completion: completion)
	}
}
