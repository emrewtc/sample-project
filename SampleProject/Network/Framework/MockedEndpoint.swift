//
//  MockedEndpoint.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

class MockedEndpoint: EndpointType {
	private let wrappee: EndpointType
	let mockData: Data
	
	init(wrappee: EndpointType, mockData: Data) {
		self.wrappee = wrappee
		self.mockData = mockData
	}
	
	var isMocked: Bool {
		return true
	}
	
	var path: String {
		return wrappee.path
	}
	
	var httpMethod: HTTPMethod {
		return wrappee.httpMethod
	}
	
	var queryParameters: [String: String?]? {
		return wrappee.queryParameters
	}
	
	var body: Data? {
		return wrappee.body
	}
	
	var headerParamaters: [String: String]? {
		return wrappee.headerParamaters
	}
	
	var timeout: TimeInterval {
		return wrappee.timeout
	}
	
	func urlRequest(for service: ApiClient) throws -> URLRequest {
		return try wrappee.urlRequest(for: service)
	}
}
