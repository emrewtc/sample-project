//
//  ServiceConfig.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

struct ServiceConfig: ServiceConfigurable {
	var headers: [String: String] = [
		"Content-Type": "application/json"
	]
	
	var baseUrl: String {
		guard let baseUrl = Bundle.main.object(forInfoDictionaryKey: "BaseURL") as? String else {
			print("Base Url is not a valid URL")
			return ""
		}
		return baseUrl
	}
	
}
