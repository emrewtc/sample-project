//
//  NetworkSession.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/2/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol NetworkSession {
	func networkSessionDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask?
}

extension URLSession: NetworkSession {
	func networkSessionDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask? {
		let task = self.dataTask(with: request, completionHandler: completionHandler)
		task.resume()
		return task
	}
	
}

final class MockNetworkSession: NetworkSession {
	private var data: Data?
	private var error: Error?
	
	func setStubData(_ data: Data) {
		self.data = data
	}
	
	func setStubError(_ error: Error) {
		self.error = error
	}
	
	func removeStubs() {
		self.data = nil
		self.error = nil
	}
	
	func networkSessionDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask? {
		completionHandler(self.data, nil, self.error)
		return nil
	}
	
}
