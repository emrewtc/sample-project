//
//  Dispatcher.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol TaskCancelable {
	func cancel()
}

extension URLSessionDataTask: TaskCancelable {}

struct Dispatcher<T> {
	private let apiClient: ApiClient
	
	init(with apiClient: ApiClient) {
		self.apiClient = apiClient
	}
	
	@discardableResult
	func request<T: Decodable>(with endpoint: Requestable, completion: @escaping (Response<T>) -> Void) -> TaskCancelable? {
		do {
			let urlRequest = try endpoint.urlRequest(for: apiClient)
			let task = apiClient.session.networkSessionDataTask(with: urlRequest) { data, _, error in
				if let error = error {
					print("API Request error: \(error.localizedDescription)")
					completion(.error(NetworkError.unknownError))
					return
				}
				guard let data = data else {
					completion(.error(NetworkError.noResponse))
					return
				}
				
				do {
					let decoder = JSONDecoder.standard
					let result = try decoder.decode(T.self, from: data)
					completion(.success(result))
				} catch let error {
					if let url = urlRequest.url?.absoluteString {
						print(url)
					}
					print("⛔️ Unable to decode data: \(String(data: data, encoding: .utf8) ?? "N/A"), description: \(error.localizedDescription)")
					completion(.error(NetworkError.failedToParseJSON))
					return
				}
			}
			task?.resume()
			return task
		} catch {
			completion(.error(NetworkError.unknownError))
			return nil
		}
	}
	
}
