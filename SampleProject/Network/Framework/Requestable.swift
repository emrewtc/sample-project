//
//  Requestable.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
	case get = "GET"
	case head = "HEAD"
	case post = "POST"
	case put = "PUT"
	case delete = "DELETE"
}

protocol Requestable {
	var path: String { get }
	var httpMethod: HTTPMethod { get }
	var queryParameters: [String: String?]? { get }
	var body: Data? { get }
	var headerParamaters: [String: String]? { get }
	var timeout: TimeInterval { get }
	
	func url(for service: ApiClient) throws -> URL
	func urlRequest(for service: ApiClient) throws -> URLRequest
	
}

extension Requestable {	
	func url(for apiClient: ApiClient) throws -> URL {
		let endpoint = apiClient.config.baseUrl.appending(path)
		guard var urlComponents = URLComponents(string: endpoint) else {
			throw NetworkError.invalidUrl
		}
		
		if let queryParameters = queryParameters {
			urlComponents.queryItems = queryParameters.map {
				URLQueryItem(name: $0, value: $1)
			}
		}
		
		guard let url = urlComponents.url else {
			throw NetworkError.invalidUrl
		}
		
		return url
	}
	
	func urlRequest(for service: ApiClient) throws -> URLRequest {
		do {
			let url = try self.url(for: service)
			
			var urlRequest = URLRequest(url: url, timeoutInterval: service.config.timeout)
			urlRequest.httpMethod = httpMethod.rawValue
			urlRequest.allHTTPHeaderFields = headerFields(with: service.config.headers)
			
			if let body = body {
				urlRequest.httpBody = body
			}
			
			return urlRequest
		} catch {
			throw NetworkError.invalidRequest
		}
	}
	
	private func headerFields(with defaultHeaders: [String: String]) -> [String: String] {
		var allHeaders = defaultHeaders
		if let headerParamaters = headerParamaters {
			headerParamaters.forEach {
				allHeaders.updateValue($1, forKey: $0)
			}
		}
		return allHeaders
	}
	
}
