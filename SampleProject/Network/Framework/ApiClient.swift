//
//  ApiClient.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol ApiClient {
	var config: ServiceConfigurable { get }
	var session: NetworkSession { get }
	
}

extension ApiClient {
	var config: ServiceConfigurable {
		return ServiceConfig()
	}
	
}

protocol ServiceConfigurable {
	var headers: [String: String] { get set }
	var timeout: TimeInterval { get }
	var baseUrl: String { get }
	
}

extension ServiceConfigurable {
	var timeout: TimeInterval {
		return 60.0
	}
}

enum NetworkError: Error, CustomStringConvertible {
	case invalidUrl
	case invalidRequest
	case unknownError
	case knownError(errorCode: Int)
	case noResponse
	case failedToParseJSON
	
	var description: String {
		switch self {
		case .invalidUrl:
			return "Unable to create URL."
		case .invalidRequest:
			return "Unable to create request."
		case .noResponse:
			return "No data in response."
		case .failedToParseJSON:
			return "Unable to parse JSON."
		case .knownError(errorCode: let code):
			return "A known network error occured. Code \(code)"
		case .unknownError:
			return "An unknown network error occured."
		}
	}
}

enum Response<T> {
	case success(T)
	case error(NetworkError)
}
