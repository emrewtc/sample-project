//
//  EndpointType.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol EndpointType: Requestable & CustomStringConvertible {}

extension EndpointType {
	var httpMethod: HTTPMethod {
		return .get
	}
	
	var queryParameters: [String: String?]? {
		return [:]
	}
	
	var body: Data? {
		return nil
	}
	var headerParamaters: [String: String]? {
		return nil
	}
	var timeout: TimeInterval {
		return 60.0
	}
	
	var description: String {
		return """
		httpMethod: \(httpMethod),
		queryParameters: \(String(describing: queryParameters)),
		headerParameters: \(String(describing: headerParamaters)),
		timeout: \(timeout)
		"""
	}
	
	func mocked(fileName: String) -> MockedEndpoint {
		let fileName = fileName as NSString
		guard let path = Bundle.main.path(forResource: fileName.deletingPathExtension, ofType: fileName.pathExtension),
			let text = try? String(contentsOfFile: path),
			let data = text.data(using: .utf8) else {
				print("NO FILE IS FOUND")
				return MockedEndpoint(wrappee: self, mockData: Data())
		}
		
		return MockedEndpoint(wrappee: self, mockData: data)
	}
}
