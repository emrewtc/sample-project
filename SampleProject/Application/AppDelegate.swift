//
//  AppDelegate.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

protocol ApplicationService: UIApplicationDelegate { }

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	private let services: [ApplicationService] = [NavigationService()]

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		self.services.forEach { _ = $0.application?(application, didFinishLaunchingWithOptions: launchOptions) }
		return true
	}

}
