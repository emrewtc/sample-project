//
//  NavigationService.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class NavigationService: NSObject, ApplicationService {
	var window: UIWindow?
	var appRouter: AppRouting?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		self.window = UIWindow(frame: UIScreen.main.bounds)
		guard let window = self.window else {
			fatalError("UIWindow wasn't initialized properly!")
		}
		self.setupGlobalAppearance()
		self.appRouter = AppRouter(with: window)
		self.appRouter?.start(with: launchOptions)
		return true
	}
	
	private func setupGlobalAppearance() {
		let navBarAppearance = UINavigationBar.appearance()
		navBarAppearance.prefersLargeTitles = true
		navBarAppearance.tintColor = .white
		navBarAppearance.barStyle = .blackTranslucent; // white text color and status bar
		navBarAppearance.titleTextAttributes = [
			NSAttributedString.Key.font: UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.black)
		]
	}
}
