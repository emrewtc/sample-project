//
//  FeedCell.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class FeedCell: UITableViewCell {
	static let reuseIdentifier = "FeedCellReuseIdentifier"
	
	var feedItemCollectionViewOffset: CGFloat {
		get {
			return self.feedItemCollectionView.contentOffset.x
		}
		set {
			self.feedItemCollectionView.contentOffset.x = newValue
		}
	}
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		self.setupConstraints()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func configure(with dataSource: UICollectionViewDataSource, delegate: UICollectionViewDelegate, offset: CGFloat = 0, tag: Int) {
		self.feedItemCollectionView.dataSource = dataSource
		self.feedItemCollectionView.delegate = delegate
		self.feedItemCollectionViewOffset = offset
		self.feedItemCollectionView.tag = tag
		self.feedItemCollectionView.reloadData()
		self.contentView.layoutIfNeeded()
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			self.feedItemCollectionView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
			self.feedItemCollectionView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
			self.feedItemCollectionView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
			self.feedItemCollectionView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)
		])
	}
	
	private lazy var feedItemCollectionView: UICollectionView = {
		let layout = UICollectionViewFlowLayout()
		layout.minimumLineSpacing = 10
		layout.scrollDirection = .horizontal
		layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
		
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
		collectionView.backgroundColor = .black
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		self.contentView.addSubview(collectionView)
		collectionView.register(FeedItemCell.self, forCellWithReuseIdentifier: FeedItemCell.reuseIdentifier)
		return collectionView
	}()
}
