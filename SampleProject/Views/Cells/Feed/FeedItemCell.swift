//
//  FeedItemCell.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class FeedItemCell: UICollectionViewCell {
	static let reuseIdentifier = "FeedItemCellReuseIdentifier"
	
	override init(frame: CGRect) {
		super.init(frame: .zero)
		self.setupConstraints()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func configure(with viewModel: Photo) {
		self.imageView.setImage(with: viewModel.media.m)
		self.titleLabel.text = viewModel.title
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		self.titleLabel.text = ""
		self.imageView.image = nil
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
			self.imageView.heightAnchor.constraint(equalToConstant: 200),
			self.imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
			self.imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)
		])
		
		NSLayoutConstraint.activate([
			self.titleLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 5),
			self.titleLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
			self.titleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10),
			self.titleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10)
		])
	}
	
	private lazy var imageView: UIImageView = {
		$0.translatesAutoresizingMaskIntoConstraints = false
		$0.contentMode = .scaleAspectFit
		self.contentView.addSubview($0)
		return $0
	}(UIImageView(frame: .zero))
	
	private lazy var titleLabel: UILabel = {
		$0.translatesAutoresizingMaskIntoConstraints = false
		$0.textAlignment = .center
		$0.textColor = .white
		self.contentView.addSubview($0)
		return $0
	}(UILabel(frame: .zero))
	
}
