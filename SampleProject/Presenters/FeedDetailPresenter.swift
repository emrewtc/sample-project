//
//  FeedDetailPresenter.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/5/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

protocol FeedDetailPresenting {
	func getTitle() -> String
	func getImageURL() -> URL
	func didClickViewOnFlickrButton()
}

final class FeedDetailPresenter: FeedDetailPresenting {
	private let viewModel: Photo
	private let router: FeedRouting
	
	init(with router: FeedRouting, viewModel: Photo) {
		self.router = router
		self.viewModel = viewModel
	}
	
	func getTitle() -> String {
		return self.viewModel.title
	}
	
	func getImageURL() -> URL {
		return self.viewModel.media.m
	}
	
	func didClickViewOnFlickrButton() {
		self.router.didClickViewOnFlickrButton(self.viewModel.link)
	}
}
