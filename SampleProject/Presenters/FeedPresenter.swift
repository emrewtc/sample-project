//
//  FeedPresenter.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol FeedPresenting {
	var didUpdateFeed: (FeedType) -> Void { get set }
	var didFailUpdatingFeed: (Error, FeedType) -> Void { get set }
	
	func didClickFeedItem(_ item: Photo)
	func getPhotoCount(for type: FeedType) -> Int
	func getPhotos(for type: FeedType) -> [Photo]
	func getTitle(for feedType: FeedType) -> String
	func getLastUpdateAt() -> String
}

final class FeedPresenter: FeedPresenting {
	var didUpdateFeed: (FeedType) -> Void = { _ in }
	var didFailUpdatingFeed: (Error, FeedType) -> Void = { _, _ in }
	private let router: FeedRouting
	private var interactor: FeedInteractingOutput
	private var feeds: Feeds = [:]
	private var feedLastModifiedDate: Date?
	
	init(with router: FeedRouting, interactor: FeedInteractingOutput) {
		self.router = router
		self.interactor = interactor
		self.observeFeed()
	}
	
	private func observeFeed() {
		self.interactor.didFetchFeed = { [weak self] result, type in
			switch result {
			case .success(let feed):
				if case .public = type {
					self?.feedLastModifiedDate = feed.modifiedDate
				}
				self?.feeds[type] = feed.items
				DispatchQueue.main.async {
					self?.didUpdateFeed(type)
				}
			case .failure(let error):
				DispatchQueue.main.async {
					self?.didFailUpdatingFeed(error, type)
				}
			}
		}
	}
	
	func didClickFeedItem(_ item: Photo) {
		self.router.didClickFeedItem(item)
	}
	
	func getPhotoCount(for type: FeedType) -> Int {
		return self.feeds[type]?.count ?? 0
	}
	
	func getPhotos(for type: FeedType) -> [Photo] {
		guard let feed = self.feeds[type] else {
			return []
		}
		return feed
	}

	func getTitle(for feedType: FeedType) -> String {
		switch feedType {
		case .public:
			return "Public Feed"
		case .cats:
			return "Cats"
		case .dogs:
			return "Dogs"
		}
	}

	func getLastUpdateAt() -> String {
		guard let updateAt = self.feedLastModifiedDate else {
			return ""
		}
		let formatter = DateFormatter()
		formatter.dateFormat = "MMM d, h:mm a"
		return formatter.string(from: updateAt)
	}
}
