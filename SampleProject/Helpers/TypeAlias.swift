//
//  TypeAlias.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]
typealias FeedRouting = FeedRouterDelegate & BaseRouting
typealias Feeds = [FeedType: [Photo]]
