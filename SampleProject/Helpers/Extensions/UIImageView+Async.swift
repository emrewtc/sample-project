//
//  UIImageView+Async.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

import UIKit

private final class ImageCacheManager {
	static let shared = ImageCacheManager()
	let cache = NSCache<NSString, UIImage>()
	
	private init() {
		self.cache.countLimit = 150
	}
}

extension UIImageView {
	func setImage(with urlString: String, placeholder: UIImage? = nil, completion: ((Bool) -> Void)? = nil) {
		guard let url = URL(string: urlString) else {
			completion?(false)
			return
		}
		self.setImage(with: url, placeholder: placeholder, completion: completion)
	}
	
	func setImage(with url: URL, placeholder: UIImage? = nil, completion: ((Bool) -> Void)? = nil) {
		if let cachedImage = ImageCacheManager.shared.cache.object(forKey: url.absoluteString as NSString) {
			self.image = cachedImage
			completion?(true)
			return
		}
		self.image = placeholder
		
		URLSession.shared.dataTask(with: url) { (data, _, error) in
			guard let data = data, error == nil, let image = UIImage.init(data: data) else {
				completion?(false)
				return
			}
			
			DispatchQueue.main.async {
				ImageCacheManager.shared.cache.setObject(image, forKey: url.absoluteString as NSString)
				self.image = image
				completion?(true)
				return
			}
		}.resume()
	}
}
