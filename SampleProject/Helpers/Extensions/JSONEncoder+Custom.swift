//
//  JSONEncoder+Custom.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

extension JSONEncoder {
	static var standard: JSONEncoder {
		let encoder = JSONEncoder()
		encoder.dateEncodingStrategy = .iso8601
		encoder.outputFormatting = .prettyPrinted
		return encoder
	}
}

extension JSONDecoder {
	static var standard: JSONDecoder {
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .iso8601
		return decoder
	}
}

extension Encodable where Self: Any {
	func asJSONData() -> Data? {
		let data = try? JSONEncoder.standard.encode(self)
		return data
	}
}
