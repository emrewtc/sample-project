//
//  Feed.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/30/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

enum FeedType: Int, CaseIterable {
	case `public`, cats, dogs
	
	var tag: String {
		switch self {
		case .public:
			return ""
		case .cats:
			return "cats"
		case .dogs:
			return "dogs"
		}
	}
}

struct Feed: Decodable {
	let items: [Photo]
	let modifiedDate: Date
	
	enum CodingKeys: String, CodingKey {
		case items, modifiedDate = "modified"
	}
}
