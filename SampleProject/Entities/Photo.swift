//
//  Photo.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

struct Photo: Decodable {
	let title: String
	let link: URL
	let media: Media
	let takenDate: Date
	
	enum CodingKeys: String, CodingKey {
		case title, link, media, takenDate = "date_taken"
	}
}

struct Media: Decodable {
	let m: URL
}
