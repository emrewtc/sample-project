//
//  FeedItemCollectionController.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class FeedItemCollectionController: NSObject {
	var didClickFeedItem: (Photo, FeedType) -> Void = { _, _ in }
	private var viewModel: Feeds = [:]
	
	func setViewModel(_ viewModel: [Photo], for type: FeedType) {
		self.viewModel[type] = viewModel
	}
	
}

// MARK: - UICollectionViewDataSource

extension FeedItemCollectionController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let type = FeedType(rawValue: collectionView.tag) else {
			return 0
		}
		return viewModel[type]?.count ?? 0
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedItemCell.reuseIdentifier, for: indexPath) as? FeedItemCell else {
			fatalError("Couldn't create FeedItemCell!")
		}
		guard let type = FeedType(rawValue: collectionView.tag), let viewModel = self.viewModel[type] else {
			return cell
		}
		cell.configure(with: viewModel[indexPath.row])
		return cell
	}
	
}

// MARK: - UICollectionViewDelegate

extension FeedItemCollectionController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let type = FeedType(rawValue: collectionView.tag), let viewModel = self.viewModel[type] else {
			return
		}
		self.didClickFeedItem(viewModel[indexPath.row], type)
	}
}

// MARK: - UICollectionViewDelegateFlowLayout

extension FeedItemCollectionController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 240, height: 240)
	}
	
}
