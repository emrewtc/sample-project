//
//  FeedViewController.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class FeedViewController: UITableViewController {
	private enum State {
		case initial, success, error
	}
	
	private var hub: FeedHubProtocol
	private var feedItemCollectionController = FeedItemCollectionController()
	private var storedOffsets: [Int: CGFloat] = [:]
	private var feedStates: [FeedType: State] = [:]
	private lazy var refresher: UIRefreshControl = {
		$0.tintColor = .white
		$0.addTarget(self, action: #selector(didPullRefresh(_:)), for: .valueChanged)
		return $0
	}(UIRefreshControl())
	
	init(with hub: FeedHubProtocol) {
		self.hub = hub
		super.init(style: .grouped)
		self.setup()
		self.tableView.register(FeedCell.self, forCellReuseIdentifier: FeedCell.reuseIdentifier)
		self.tableView.separatorStyle = .none
		self.tableView.refreshControl = self.refresher
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Feed"
		self.view.backgroundColor = .black
	}
	
	private func setup() {
		FeedType.allCases.forEach {
			self.updateFeedState(.initial, for: $0)
		}
		self.hub.didUpdateFeed = { [weak self] type in
			self?.updateFeedState(.success, for: type)
			self?.tableView.reloadSections(IndexSet(integer: type.rawValue), with: .automatic)
			self?.refresher.endRefreshing()
		}
		
		self.hub.didFailUpdatingFeed = { [weak self] error, type in
			self?.updateFeedState(.error, for: type)
			self?.tableView.reloadSections(IndexSet(integer: type.rawValue), with: .automatic)
			self?.refresher.endRefreshing()
			print("\(type): " + error.localizedDescription)
		}
		
		self.feedItemCollectionController.didClickFeedItem = { [weak self] feedItem, _ in
			self?.hub.didClickFeedItem(feedItem)
		}
		self.fetchFeed()
	}
	
	private func fetchFeed() {
		FeedType.allCases.forEach {
			self.hub.fetchFeed(for: $0)
		}
		self.storedOffsets.removeAll()
	}
	
	@objc private func didPullRefresh(_ sender: UIControl) {
		self.refresher.attributedTitle = NSAttributedString(string: "Last update at: \(self.hub.getLastUpdateAt())")
		self.refresher.beginRefreshing()
		self.fetchFeed()
	}
	
	private func updateFeedState(_ state: State, for type: FeedType) {
		self.feedStates[type] = state
	}
	
	private func prepareFeedCellFor(indexPath: IndexPath) -> FeedCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.reuseIdentifier, for: indexPath) as? FeedCell else {
			fatalError("Couldn't create FeedCell!")
		}
		guard let feedType = FeedType(rawValue: indexPath.section) else {
			fatalError("Unknown Feed Type!")
		}
		
		let feedItemViewModel = self.hub.getPhotos(for: feedType)
		self.feedItemCollectionController.setViewModel(feedItemViewModel, for: feedType)
		let offset = self.storedOffsets[indexPath.section] ?? 0
		cell.configure(with: feedItemCollectionController, delegate: feedItemCollectionController, offset: offset, tag: indexPath.section)
		return cell
	}
	
	private func prepareErrorCell() -> UITableViewCell {
		let cell = UITableViewCell()
		cell.backgroundColor = .black
		cell.selectionStyle = .none
		cell.textLabel?.textColor = .white
		cell.textLabel?.textAlignment = .center
		cell.textLabel?.text = "An Error occured, please try again!"
		return cell
	}
}

// MARK: - TableView Data Source

extension FeedViewController {
	override func numberOfSections(in tableView: UITableView) -> Int {
		return FeedType.allCases.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let feedType = FeedType(rawValue: indexPath.section), let feedState = self.feedStates[feedType] else {
			print("Unknown Feed Type!")
			return UITableViewCell()
		}

		switch feedState {
		case .initial, .success:
			return self.prepareFeedCellFor(indexPath: indexPath)
		case .error:
			return self.prepareErrorCell()
		}
	}
}

// MARK: - TableView Delegate

extension FeedViewController {
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let feedType = FeedType(rawValue: section) else {
			return nil
		}
		return self.hub.getTitle(for: feedType)
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 260
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 40
	}
	
	override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		guard let cell = cell as? FeedCell else {
			return
		}
		self.storedOffsets[indexPath.section] = cell.feedItemCollectionViewOffset
	}
}
