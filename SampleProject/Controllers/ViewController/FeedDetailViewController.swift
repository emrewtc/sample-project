//
//  FeedDetailViewController.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/5/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

final class FeedDetailViewController: UIViewController {
	private let hub: FeedDetailHubProtocol
	
	init(with hub: FeedDetailHubProtocol) {
		self.hub = hub
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		super.loadView()
		self.view.backgroundColor = .black
		_ = self.imageView
		_ = self.titleLabel
		_ = self.flickrButton
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setup()
	}
	
	private func setup() {
		let imageURL = self.hub.getImageURL()
		self.imageView.setImage(with: imageURL)
		self.titleLabel.text = self.hub.getTitle()
	}
	
	@objc private func didClickFlickrButton(_ sender: UIButton) {
		self.hub.didClickViewOnFlickrButton()
	}
	
	private lazy var imageView: UIImageView = {
		$0.contentMode = .scaleAspectFit
		$0.translatesAutoresizingMaskIntoConstraints = false
		self.view.addSubview($0)
		
		NSLayoutConstraint.activate([
			$0.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
			$0.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4),
			$0.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
			$0.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
			])
		return $0
	}(UIImageView(frame: .zero))
	
	private lazy var titleLabel: UILabel = {
		$0.textColor = .white
		$0.textAlignment = .center
		$0.translatesAutoresizingMaskIntoConstraints = false
		self.view.addSubview($0)
		
		NSLayoutConstraint.activate([
			$0.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 30),
			$0.heightAnchor.constraint(equalToConstant: 60),
			$0.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 50),
			$0.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -50)
			])
		
		return $0
	}(UILabel(frame: .zero))
	
	private lazy var flickrButton: UIButton = {
		$0.backgroundColor = .white
		$0.setTitle("View On Flickr", for: .normal)
		$0.addTarget(self, action: #selector(didClickFlickrButton(_:)), for: .touchUpInside)
		$0.tintColor = .black
		
		$0.translatesAutoresizingMaskIntoConstraints = false
		self.view.addSubview($0)
		
		NSLayoutConstraint.activate([
			$0.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 50),
			$0.heightAnchor.constraint(equalToConstant: 40),
			$0.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 50),
			$0.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -50)
			])
		
		return $0
	}(UIButton(type: .system))
	
}
