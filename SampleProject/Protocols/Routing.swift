//
//  Routing.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 4/29/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

protocol BaseRouting: class {
	func start()
}

protocol Routing: BaseRouting {
	associatedtype ViewController: UIViewController
	var viewController: ViewController? { get set }
	var root: UINavigationController { get }
	
	func createViewController() -> ViewController
	func configure(viewController: ViewController)
	func show(viewController: ViewController)
	func present(viewController: ViewController, animated: Bool, completion: (() -> Void)?)
	func dismiss(animated: Bool, completion: (() -> Void)?)
}

extension Routing {
	func start() {
		let viewController = self.createViewController()
		self.configure(viewController: viewController)
		self.show(viewController: viewController)
		self.viewController = viewController
	}
	
	func configure(viewController: ViewController) { }
	
	func show(viewController: ViewController) {
		self.root.show(viewController, sender: self.root)
	}
	
	func present(viewController: ViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
		self.root.present(viewController, animated: animated, completion: completion)
	}
	
	func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
		self.root.dismiss(animated: animated, completion: completion)
	}
}

protocol AppRouting: BaseRouting {
	var window: UIWindow { get }
	var launchOptions: LaunchOptions? { get set }
	
	func start(with launchOptions: LaunchOptions?)
}

extension AppRouting {
	func start() { }
}
