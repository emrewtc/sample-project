//
//  FeedHub.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/1/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import Foundation

protocol FeedHubProtocol: FeedPresenting, FeedInteracingInput { }

final class FeedHub: FeedHubProtocol {
	var didUpdateFeed: (FeedType) -> Void = { _ in } {
		didSet {
			self.presenter.didUpdateFeed = self.didUpdateFeed
		}
	}
	
	var didFailUpdatingFeed: (Error, FeedType) -> Void  = { _, _ in } {
		didSet {
			self.presenter.didFailUpdatingFeed = self.didFailUpdatingFeed
		}
	}
	private let interactor: FeedInteracingInput
	private var presenter: FeedPresenting
	
	init(with interactor: FeedInteracingInput, presenter: FeedPresenting) {
		self.interactor = interactor
		self.presenter = presenter
	}
	
	func didClickFeedItem(_ viewModel: Photo) {
		self.presenter.didClickFeedItem(viewModel)
	}
	
	func getPhotoCount(for type: FeedType) -> Int {
		return self.presenter.getPhotoCount(for: type)
	}
	
	func getPhotos(for type: FeedType) -> [Photo] {
		return self.presenter.getPhotos(for: type)
	}

	func getTitle(for feedType: FeedType) -> String {
		return self.presenter.getTitle(for: feedType)
	}

	func getLastUpdateAt() -> String {
		self.presenter.getLastUpdateAt()
	}

	func fetchFeed(for type: FeedType) {
		self.interactor.fetchFeed(for: type)
	}
	
}
