//
//  FeedDetailHub.swift
//  SampleProject
//
//  Created by Emre Cakirlar on 5/5/19.
//  Copyright © 2019 emrewtc. All rights reserved.
//

import UIKit

protocol FeedDetailHubProtocol: FeedDetailPresenting { }

final class FeedDetailHub: FeedDetailHubProtocol {
	private let presenter: FeedDetailPresenting
	
	init(with presenter: FeedDetailPresenting) {
		self.presenter = presenter
	}
	
	func getTitle() -> String {
		return self.presenter.getTitle()
	}
	
	func getImageURL() -> URL {
		return self.presenter.getImageURL()
	}
	
	func didClickViewOnFlickrButton() {
		self.presenter.didClickViewOnFlickrButton()
	}
	
}
